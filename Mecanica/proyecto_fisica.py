# Programa para el calculo de multas
import math

def perf_inelastico(a, b, c):
  print("Ha escogido un choque donde los autos permanecieron juntos")
  d = float(input("Ingrese de la velocidad en kilometros por hora de los autos, luego del choque"))
  e = float(input("Ingrese el angulo medido desde la via con el sensor hasta el sitio final de los autos luego del choque en dirección antihoraria, y medido en grados"))
  # Transforforma de grados a radianes
  e = math.radians(e)
  e = math.sin(e)
  # Transformar en metros sobre segundo
  d = (d * 1000) / 3600
  d = d * e
  # Devuelve el valor de V2 en kilometros por hora
  return round((((d * (a + b) * e) / b) * 3600) / 1000)

def inelastico(a, b, c):
  print("Ha escogido un choque donde los autos han quedado separados")
  f = float(input("Ingrese la velocidad en kilometros por hora, del auto que va por la via con el sensor luego del choque"))
  g = float(input("Ingrese la velocidad en kilometros por hora, del auto que va por la via perpendicular luego del choque"))
  h = float(input("Ingrese el angulo medido desde la via con el sensor hasta el auto que iba en la via con el sensor antes del choque en dirección antihoraria, y medido en grados"))
  i = float(input("Ingrese el angulo medido desde la via con el sensor hasta el auto que iba en la via perpendicular antes del choque en dirección antihoraria, y medido en grados"))
  f = (f * 1000) /3600
  g = (g * 1000) /3600
  h = math.radians(h)
  h = math.sin(h)
  i = math.radians(i)
  i = math.sin(i)
  vf1 = f * h
  vf2 = g * i
  return round(((((a * vf1 * h) + (b * vf2 * i)) / b) * 3600) / 1000)
  

def main():
  print("Bienvenido a su programa para calcular multas")
  print("->")
  a = float(input("Ingrese el peso en kilogramos del auto que va por la via con el sensor"))
  b = float(input("Ingrese el peso en kilogramos del auto que va por la via perpendicular"))
  c = float(input("Ingrese la velocidad en kilometros por hora del auto que va por la via con el sensor"))
  d = 0
  while d != "si" and d != "no":
    d = input("Los autos estan separados si o no, conteste con las palabras si o no")
    if d == "si":
      respuesta = inelastico(a, b, c)
    elif d == "no":
      respuesta = perf_inelastico(a, b, c)
      break
    else:
      print("El valor no es valido y se repetira la pregunta")
  print(f"La velocidad del auto en la via perpendicular es de {respuesta} kilometros por hora ")
  if c > 80 and respuesta > 80:
    print("Ambos autos violan el limite de velocidad")
  elif c > 80 and respuesta <= 80:
    print("El auto que va sobre la via con el sensor viola el limite de velocidad")
  elif c <= 80 and respuesta > 80:
    print("El auto que va sobre la via perpendicular viola el limite de velocidad")
  else:
    print("No se violan los limites de velocidad") 

   
  
main()
