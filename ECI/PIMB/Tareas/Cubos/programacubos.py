# Este programa calcula la cantidad de cubos
# Autor: David Julian Tete Mieles
# Fecha: 22 de Agosto de 2018

def main():
    lado_cubo = int(input("Ingrese el lado en centimetros de un cubo a producir"))
    cant_cubos = int(input("Ingrese la cantidad de cubos a producir"))
    ancho_t = int(input("Ingrese el ancho en centimetros del tablon del proveedor"))
    largo_t = int(input("Ingrese el largo en centimetros del tablon del proveedor"))
    altura_t = int(input("Ingrese la altura en centimetros del tablon del proveedor"))
    peso_caja = float(input("Ingrese el peso maximo en kilogramos de caja de dados"))
    densidad_cedro = 490
    volumen_cubo = lado_cubo ** 3
    masa_cubo = densidad_cedro * volumen_cubo
    masa_caja = peso_caja / 9.8
    volumen_t = ancho_t * largo_t * altura_t
    print("El volumen del tablon en centimetros cubicos es", volumen_t)
    cubos_por_t = volumen_t / volumen_cubo
    pedido_al_proveedor = cant_cubos / cubos_por_t
    print("La cantidad de tablones a pedir al proveedor es", pedido_al_proveedor)
    volumen_caja = densidad_cedro * masa_caja
    cantidad_cubos_por_caja = volumen_caja / volumen_cubo
    print("La cantidad de cubos por caja es", cantidad_cubos_por_caja)
    cant_cajas_pedido = cant_cubos / cantidad_cubos_por_caja
    print("La cantidad de cajas para realizar el pedido es de", cant_cajas_pedido)
main()
