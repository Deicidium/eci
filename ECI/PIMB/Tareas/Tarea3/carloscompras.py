# Tarea 3. Cometas y Compras
# Autor : David Julian Tete Mieles
# Agosto 27 de 2018
import math

def main():
    frutascarlos = int(input("Ingrese la cantidad de frutas que compró Carlos"))
    if frutascarlos >= 30 and frutascarlos <= 50:
        manzanascarlos = round(frutascarlos * 0.33)
        aguacatescarlos = round(frutascarlos * 0.25)
        perascarlos = round(frutascarlos * 0.167)
        naranjascarlos = round(frutascarlos * 0.125)
        fresascarlos = frutascarlos - (manzanascarlos + aguacatescarlos + perascarlos + naranjascarlos)
        print("Carlos primero tiene esta cantidad de manzanas", manzanascarlos)
        print("Carlos primero tiene esta cantidad de aguacates", aguacatescarlos)
        print("Carlos primero tiene esta cantidad de peras", perascarlos)
        print("Carlos primero tiene esta cantidad de naranjas", naranjascarlos)
        print("Carlos primero tiene esta cantidad de fresas", fresascarlos)
        amigacarlos = round(frutascarlos * 0.5)
        frutascarlos = frutascarlos + amigacarlos
        print("Con el aporte de su amiga ahora Carlos tiene esta cantidad de frutas", frutascarlos)
        manzanasdespensa = round(frutascarlos * 0.333)
        fresasdespensa = round(frutascarlos * 0.5)
        aguacatesdespensa = round(frutascarlos * 0.055)
        perasdespensa = round(frutascarlos * 0.055)
        naranjasdespensa = round(frutascarlos * 0.055)
        print("Carlos tiene esta cantidad de manzanas en la depensa", manzanasdespensa)
        print("Carlos tiene esta cantidad de fresas en la depensa", fresasdespensa)
        print("Carlos tiene esta cantidad de aguacates en la depensa", aguacatesdespensa)
        print("Carlos tiene esta cantidad de peras en la depensa", perasdespensa)
        print("Carlos tiene esta cantidad de naranjas en la depensa", naranjasdespensa)
    else:
        print("El numero de frutas no corresponde con los parametros del problema")

main()

    
    
    
    


    
