# Tarea 4. Funciones. Punto 2.
# Autor : David Julian Tete Mieles
# Septiembre 03 de 2018

import math

def main():
    print("Responda a las preguntas como Si o No, literalmente cuando sea el caso")
    tipo_acento = input("Si la palabra es un aguda, escriba aguda; si es grave, escriba grave")
    if tipo_acento == "aguda":
        primera_aguda = input("La palabra termina en n o s, responda Si o No")
        if primera_aguda == "Si":
            segunda_aguda = input("La palabra termina en s, pero no esta precedida de una consonante")
            if segunda_aguda == "Si":
                print("Es una palabra aguda que lleva tilde")
            elif segunda_aguda == "No":
                print("Es una palabra aguda que no lleva tilde")
            else:
                print("No respondio de forma adecuada la pregunta")
        elif primera_aguda == "No":
            tercera_aguda = print("La palabra termina en vocal, responda Si o No")
            if tercera_aguda == "Si":
                print("Es una palabra aguda lleva tilde")
            elif tercera_aguda == "No":
                print("Es una palabra aguda que no lleva tilde")
            else:
                print("No respondio de forma adecuada la pregunta")
        else:
            print("No respondio de forma adecuada la pregunta")
    elif tipo_acento == "grave":
        primera_grave = print("La palabra termina en n, responda Si o No")
        if primera_grave == "Si":
            print("Es una palabra grave que no lleva tilde")
        elif primera_grave == "No":
            print("Es una palabra grave que lleva tilde")
        else:
            segunda_grave = input("Es una palabra grave que termina en s, y esta precedida de consonante, responda Si o No")
            if segunda_grave == "Si":
                print("Es una palabra grave que lleva tilde")
            elif segunda_grave == "No":
                print("Es una palabra grave que no lleva tilde")
            else:
                print("No respondio de forma adecuada la pregunta")
    else:
        print("No respondio de forma adecuada la pregunta")
            
                
main()
    
# Para comparar cadenas, toca agregarles comillas en la operacion booleana, ver linea 10
