# Tarea 4. Funciones. Punto 5.
# Autor : David Julian Tete Mieles
# Septiembre 02 de 2018

import math

def main():
    x = float(input("Ingrese el numero el numero x"))
    if x < 1:
        print(str(x * (-1)))
    elif x < 2 and x >= 1:
        print(str(math.sin(x)))
    else:
        print(str(math.cos(x)))
main()
    
