# Tarea 4. Funciones. Punto 4.
# Autor : David Julian Tete Mieles
# Septiembre 02 de 2018

def main():
    hora = int(input("Ingrese la hora en formato de 24 horas así 00-23"))
    minutos = int(input("Ingrese los minutos en formato de 24 horas así 00-60"))
    segundos = int(input("Ingrese los segundos en formato de 24 horas así 00-60"))
    if segundos < 59 and minutos <= 59 and hora <= 23:
        segundos = segundos + 1
    elif segundos == 59:
        segundos = 00
        minutos = minutos + 1
        if minutos == 60:
            minutos = 00
            hora = hora +1
        else:
            minutos = minutos
        if hora == 24:
            hora = 00
        else:
            hora = hora
    
    else:
        print("El numero de hora es incorrecto")
    print(str(hora)+","+str(minutos)+","+str(segundos))

main()
    
