# Tarea 4. Funciones. Punto 1.
# Autor : David Julian Tete Mieles
# Septiembre 02 de 2018

def main():
    numero = input("Ingrese el numero de tarjeta de credito")
    if len(numero) != 10:
        print("Este numero no pertenece a una tarjeta de credito")
    else:
        numero = int(numero)
        primer_digito = numero // 1000000000
        primer_residuo = numero % 1000000000
        segundo_digito = primer_residuo // 100000000
        segundo_residuo = primer_residuo % 100000000
        tercer_digito = segundo_residuo // 10000000
        tercer_residuo = segundo_residuo % 10000000
        cuarto_digito = tercer_residuo // 1000000
        cuarto_residuo = tercer_residuo % 1000000
        quinto_digito = cuarto_residuo // 100000
        quinto_residuo = cuarto_residuo % 100000
        sexto_digito = quinto_residuo // 10000
        sexto_residuo = quinto_residuo % 10000
        septimo_digito = sexto_residuo // 1000
        septimo_residuo = sexto_residuo % 1000
        octavo_digito = septimo_residuo // 100
        octavo_residuo = septimo_residuo % 100
        noveno_digito = octavo_residuo // 10
        noveno_residuo = octavo_residuo % 10
        decimo_digito = noveno_residuo // 1
        fd1a = (segundo_digito * 2)
        if fd1a >= 4:
            primer_digitofd1a = fd1a // 10
            primer_residuofd1a = fd1a % 10
            segundo_digitofd1a = primer_residuofd1a // 1
            fd1 = primer_digitofd1a + segundo_digitofd1a
        else:
            fd1 = fd1a
        fd3a = (cuarto_digito * 2)
        if fd3a >= 4:
            primer_digitofd3a = fd3a // 10
            primer_residuofd3a = fd3a % 10
            segundo_digitofd3a = primer_residuofd3a // 1
            fd3 = primer_digitofd3a + segundo_digitofd3a
        else:
            fd3 = fd3a
        fd5a = (sexto_digito * 2)
        if fd5a >= 4:
            primer_digitofd5a = fd5a // 10
            primer_residuofd5a = fd5a % 10
            segundo_digitofd5a = primer_residuofd5a // 1
            fd5 = primer_digitofd5a + segundo_digitofd5a
        else:
            fd5 = fd5a
        fd7a = (octavo_digito * 2)
        if fd7a >= 4:
            primer_digitofd7a = fd7a // 10
            primer_residuofd7a = fd7a % 10
            segundo_digitofd7a = primer_residuofd7a // 1
            fd7 = primer_digitofd7a + segundo_digitofd7a
        else:
            fd7 = fd7a
        fd9a = (decimo_digito * 2)
        if fd9a >= 4:
            primer_digitofd9a = fd9a // 10
            primer_residuofd9a = fd9a % 10
            segundo_digitofd9a = primer_residuofd9a // 1
            fd9 = primer_digitofd9a + segundo_digitofd9a
        else:
            fd9 = fd9a
        print(numero)
        print(primer_digito)
        print(fd1)
        print(tercer_digito)
        print(fd3)
        print(quinto_digito)
        print(fd5)
        print(septimo_digito)
        print(fd7)
        print(noveno_digito)
        print(fd9)
        print(primer_digito + fd1 + tercer_digito + fd3 + quinto_digito + fd5 + septimo_digito + fd7 + noveno_digito + fd9)
        if (primer_digito + fd1 + tercer_digito + fd3 + quinto_digito + fd5 + septimo_digito + fd7 + noveno_digito + fd9) % 10 == 0:
            print("Es un numero de tarjeta de credito valido")
        else:
            print("Este numero no pertenece a una tarjeta de credito")
        
        
main()
    
