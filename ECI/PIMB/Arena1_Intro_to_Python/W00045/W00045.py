# W00045 - Determinante
# Autor: David Julian Tete Mieles
# 28 de Agosto de 2018

from sys import stdin

def main():
    e = stdin.readline().strip()
    f = stdin.readline().strip()
    a, b = e.split()
    c, d = f.split()
    a = int(a)
    b = int(b)
    c = int(c)
    d = int(d)
    determinante = a * d - b * c
    print("|A|="+str(determinante))
main()
