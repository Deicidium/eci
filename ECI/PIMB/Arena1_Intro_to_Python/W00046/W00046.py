# W00046 - Triangle Type
# Autor: David Julian Tete Mieles
# 28 de Agosto de 2018

from sys import stdin

def main():
    l1 = int(stdin.readline())
    l2 = int(stdin.readline())
    l3 = int(stdin.readline())
    if (l1 + l2 <= l3) or (l1 + l3 <= l2) or (l2 + l3 <= l1):
        print("Triangulo Imposible")
    elif l1 == l2 == l3:
        perimetro = l1 + l2 + l3
        print("Equilatero, Perimetro es "+str(perimetro))
    elif (l1 == l2 and l1 != l3):
        diferencia = (l1*l2) - l3
        print("Isosceles, Diferencia es "+str(diferencia))
    elif (l1 == l3 and l1 != l2):
        diferencia = (l1*l3) - l2
        print("Isosceles, Diferencia es "+str(diferencia))
    elif (l2 == l3 and l2 != l1):
        diferencia = (l2*l3) - l1
        print("Isosceles, Diferencia es "+str(diferencia))
    elif (l1 != l2 and l1 != l3 and l2 != l3):
        if (l1 > l2 and l1 > l3):
            print("Escaleno, Lado mas largo es "+str(l1))
        elif (l2 > l1 and l2 > l3):
            print("Escaleno, Lado mas largo es "+str(l2))
        else:
            print("Escaleno. Lado mas largo es "+str(l3))
main()
        
