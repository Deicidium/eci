# 11172C - relational
# Autor: David Julian Tete Mieles
# 10 de Septiembre de 2018

from sys import stdin

def main():
    primer_numero = int(stdin.readline().strip())
    segundo_numero = int(stdin.readline().strip())
    if primer_numero < segundo_numero:
        print("<")
    elif primer_numero > segundo_numero:
        print(">")
    elif primer_numero == segundo_numero:
        print("=")
    else:
        print("La entrada no es valida")
main()
