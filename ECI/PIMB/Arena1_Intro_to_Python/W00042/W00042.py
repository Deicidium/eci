# W00042 - Multiplies
# Autor: David Julian Tete Mieles
# 10 de Septiembre de 2018

from sys import stdin

def main():
    primer_numero = int(stdin.readline())
    segundo_numero = int(stdin.readline())
    if primer_numero % segundo_numero == 0:
        print("SI")
    elif primer_numero % segundo_numero != 0:
        if segundo_numero % primer_numero == 0:
            print("SI")
        else:
            print("NO")
    else:
        print("La entrada no es valida")

main()
        

