# W00041 - Max Digit
# Autor: David Julian Tete Mieles
# 28 de Agosto de 2018

from sys import stdin

def main():
    numero = int(stdin.readline())
    primer_digito = numero // 1000
    primer_residuo = numero % 1000
    segundo_digito = primer_residuo // 100
    segundo_residuo = primer_residuo % 100
    tercer_digito = segundo_residuo // 10
    tercer_residuo = segundo_residuo % 10
    cuarto_digito = tercer_residuo // 1
    if primer_digito >= segundo_digito and primer_digito >= tercer_digito and primer_digito >= cuarto_digito:
        print(primer_digito)
    elif segundo_digito >= tercer_digito and segundo_digito >= cuarto_digito:
        print(segundo_digito)
    elif tercer_digito >= cuarto_digito:
        print(tercer_digito)
    else:
        print(cuarto_digito)
main()
