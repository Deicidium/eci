# Problema 00001 de la Arena 1: Intro to Python
# Autor: David Julian Tete Mieles
# Fecha: 28 de Agosto 2018

from sys import stdin

def main():
    p = int(stdin.readline())
    q = int(stdin.readline())
    if p < 0:
        cadena1 = "("+str(p)+")"
    else:
        cadena1 = p
    if q < 0:
        cadena2 = "("+str(q)+")"
    else:
        cadena2 = q
    r = p + q
    print(str(cadena1)+"+"+str(cadena2)+"="+str(r))
main() 
