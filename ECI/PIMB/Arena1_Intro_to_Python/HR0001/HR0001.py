# HR0001 - Compare the Triplets
# Autor: David Julian Tete Mieles
# 10 de Septiembre de 2018

from sys import stdin

def main():
    primera_cadena = stdin.readline().strip()
    segunda_cadena = stdin.readline().strip()
    primera_cadena_separada = primera_cadena.split()
    segunda_cadena_separada = segunda_cadena.split()
    a1, a2, a3 = primera_cadena_separada
    b1, b2, b3 = segunda_cadena_separada
    a1 = int(a1)
    a2 = int(a2)
    a3 = int(a3)
    b1 = int(b1)
    b2 = int(b2)
    b3 = int(b3)
    puntajea = 0
    puntajeb = 0
    if a1 > b1:
        puntajea = puntajea +1
    elif a1 == b1:
        puntajea = puntajea
        puntajeb = puntajeb
    elif a1 < b1:
        puntajeb = puntajeb +1
    else:
        print("La entrada no es valida")
    puntaje_primer_numero_a = puntajea
    puntaje_primer_numero_b = puntajeb
    puntajea = 0
    puntajeb = 0
    if a2 > b2:
        puntajea = puntajea +1
    elif a2 == b2:
        puntajea = puntajea
        puntajeb = puntajeb
    elif a2 < b2:
        puntajeb = puntajeb +1
    else:
        print("La entrada no es valida")
    puntaje_segundo_numero_a = puntajea
    puntaje_segundo_numero_b = puntajeb
    puntajea = 0
    puntajeb = 0
    if a3 > b3:
        puntajea = puntajea +1
    elif a3 == b3:
        puntajea = puntajea
        puntajeb = puntajeb
    elif a3 < b3:
        puntajeb = puntajeb +1
    else:
        print("La entrada no es valida")
    puntaje_tercer_numero_a = puntajea
    puntaje_tercer_numero_b = puntajeb
    puntaje_total_a = puntaje_primer_numero_a + puntaje_segundo_numero_a + puntaje_tercer_numero_a
    puntaje_total_b = puntaje_primer_numero_b + puntaje_segundo_numero_b + puntaje_tercer_numero_b
    print(str(puntaje_total_a)+" "+str(puntaje_total_b))

main()
        

